#ifndef READER_H
#define READER_H

#include <QObject>
#include <QSocketNotifier>

class Reader : public QObject
{
    Q_OBJECT
public:
    explicit Reader(int fd, QObject *parent = 0);

signals:

private slots:
    void readFromFd();

private:
    int m_fd;
    QSocketNotifier *m_notifier;
};

#endif // READER_H
