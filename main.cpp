#include <QCoreApplication>
#include "gpio.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    GPIO gpio(9);
    gpio.exec();

    return a.exec();
}
