#ifndef GPIO_H
#define GPIO_H

#include <QObject>
#include "reader.h"

class GPIO : public QObject
{
    Q_OBJECT
public:
    explicit GPIO(int pin, QObject *parent = 0);
    void exec();

private:
    int exportGpio(int pin);

    Reader * m_reader;
    int m_pin;
};

#endif // GPIO_H
