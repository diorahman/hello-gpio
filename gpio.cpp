#include "gpio.h"
#include <unistd.h>
#include <fcntl.h>
#include <QDebug>

GPIO::GPIO(int pin, QObject *parent) : QObject(parent), m_pin(pin)
{
}

void GPIO::exec()
{
    exportGpio(m_pin);
    int fd = ::open(QString("/sys/class/gpio/gpio%1_pe11/value").arg(m_pin).toStdString().c_str(), O_RDONLY);
    if (fd == -1) {
        qDebug() << "Error open fd";
        return;
    }
    qDebug() << "fd: " << fd << " from pin: " << m_pin;
    m_reader = new Reader(fd, this);
}

int GPIO::exportGpio(int pin)
{
#define BUFFER_MAX 3
    char buffer[BUFFER_MAX];
    ssize_t bytes_written;
    int fd;

    fd = ::open("/sys/class/gpio/export", O_WRONLY);
    if (-1 == fd) {
        return -1;
    }

    bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
    ::write(fd, buffer, bytes_written);
    ::close(fd);
    return 0;
}

