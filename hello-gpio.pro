#-------------------------------------------------
#
# Project created by QtCreator 2015-09-18T13:31:03
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = hello-gpio
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    reader.cpp \
    gpio.cpp

HEADERS += \
    reader.h \
    gpio.h

target.path = /home/adpermana/qt
INSTALLS += target
