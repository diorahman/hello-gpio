#include "reader.h"
#include <unistd.h>

#include <QDebug>

Reader::Reader(int fd, QObject *parent)
    : QObject(parent), m_fd(fd), m_notifier(NULL)
{
    qDebug() << "fd: " << m_fd;
    m_notifier = new QSocketNotifier(m_fd, QSocketNotifier::Read, this);
    connect(m_notifier, &QSocketNotifier::activated, this, &Reader::readFromFd);
}

void Reader::readFromFd()
{
    QByteArray buffer(4096, Qt::Uninitialized);
    m_notifier->setEnabled(false);

    ssize_t size = ::read(m_fd, buffer.data(), buffer.size());

    if (size > 0) {
        buffer.resize(size);
        qDebug() << "size: " << size << " content: " << QString(buffer);
    }
    m_notifier->setEnabled(true);
}
